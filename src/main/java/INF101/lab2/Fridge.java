package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    private int maxAmountInFridge = 20;
    private List<FridgeItem> itemsInFridge;

    public Fridge(){
        itemsInFridge = new ArrayList<FridgeItem>();
    }
    
    public int nItemsInFridge() {
        return itemsInFridge.size();
    }

    public int totalSize() {
        return maxAmountInFridge;
    }

    public boolean placeIn(FridgeItem item){
        if (nItemsInFridge() < totalSize()) {
            itemsInFridge.add(item);

            return true;
        }
        else {
            return false;
        }
    }
    
    public void takeOut(FridgeItem item) {
        if (itemsInFridge.contains(item)) {
            itemsInFridge.remove(item);

        }
        else {
            throw new NoSuchElementException();
        }
        
    }

    public void emptyFridge() {
        itemsInFridge.clear();
    }

    public List<FridgeItem> removeExpiredFood() {

        List<FridgeItem> expiredFoodList = new ArrayList<>();
        for (FridgeItem item: itemsInFridge) {
            if (item.hasExpired()) {
                expiredFoodList.add(item);
            }   
        }
        for (FridgeItem item: expiredFoodList){
                itemsInFridge.remove(item);
            }
        return expiredFoodList;
    }
}